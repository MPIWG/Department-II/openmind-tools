FROM python:3.10

# install pipenv
RUN pip3 install pipenv

COPY . /app
WORKDIR /app

# install app dependencies
RUN pipenv install --deploy --system

ENTRYPOINT ["python3"]
