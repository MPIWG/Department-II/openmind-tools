import networkx as nx
import csv
import sys
import pickle
import logging
import argparse

__version__ = '1.2'

## configure behaviour

# name of type attribute
node_type_attribute = '_type'
rel_type_attribute = '_type'


def get_node_fields(nx_graph):
    """returns a set of field names for nodes from nx_graph"""
    
    logging.debug("creating node field list")
    ismi_fields = set()
    # collect types of all nodes
    for node_id in nx_graph.nodes:
        attrs = nx_graph.nodes[node_id]
        # save all attribute names
        for att in attrs.keys():
            ismi_fields.add(att)
        
    return list(ismi_fields)


def get_relation_fields(nx_graph):
    """returns a set of field names for relations from nx_graph"""
    
    logging.debug("creating node field list")
    ismi_fields = set()
    # collect types of all relations
    for nx_edge in nx_graph.edges:
        # get attributes of edge
        attrs = nx_graph.edges[nx_edge]['attr_dict'].copy()
        # save all attribute names
        for att in attrs.keys():
            ismi_fields.add(att)
        
    return list(ismi_fields)


def write_nodes(nx_graph, node_writer):
    """Write all nodes from nx_graph to node_writer.
    Returns dict of ismi_ids."""
    
    logging.info("Writing nodes to CSV file")
    cnt = 0
    n4j_nodes = {}
    for node_id in nx_graph.nodes:
        attrs = nx_graph.nodes[node_id].copy()
        # get entity type
        ntype = attrs[node_type_attribute]
        # add as label
        attrs[':LABEL'] = ntype
        
        # get ismi_id
        ismi_id = attrs['ismi_id']
        # add ismi_id as node id
        attrs[':ID'] = ismi_id
        
        attrs['test'] = None
        # write row
        node_writer.writerow(attrs)
        
        # save node id
        n4j_nodes[ismi_id] = ismi_id

        cnt += 1
        if cnt % 100 == 0:
            logging.debug(f"  {cnt} nodes")
            
    logging.info(f"{cnt} nodes written")
    return n4j_nodes


def write_relations(nx_graph, n4j_nodes, relation_writer):
    """Write all relations from nx_graph to node_writer."""
    
    logging.info("Writing relations to CSV file")
    cnt = 0
    for nx_edge in nx_graph.edges:
        (nx_src, nx_tar, x) = nx_edge
        # get attributes of edge
        attrs = nx_graph.edges[nx_edge]['attr_dict'].copy()
        # get relation type
        rtype = attrs[rel_type_attribute]
        # add as label
        attrs[':TYPE'] = rtype
        
        # get ismi_id of source and target nodes
        src_id = nx_graph.nodes[nx_src]['ismi_id']
        tar_id = nx_graph.nodes[nx_tar]['ismi_id']
        # check Neo4J nodes
        src = n4j_nodes.get(src_id, None)
        if src is None:
            logging.error(f"src node {src_id} missing!")
            break
        
        tar = n4j_nodes.get(tar_id, None)
        if tar is None:
            logging.error(f"tar node {tar_id} missing!")
            break
        
        # use as start and end id
        attrs[':START_ID'] = src_id
        attrs[':END_ID'] = tar_id

        # write row
        relation_writer.writerow(attrs)
        
        cnt += 1
        if cnt % 100 == 0:
            logging.debug(f"  {cnt} relations")

    logging.info(f"{cnt} relations written")


## main
argp = argparse.ArgumentParser(description='Copy graph from networkx pickle to Neo4J import files.')
argp.add_argument('--version', action='version', version='%(prog)s ' + __version__)
argp.add_argument('input_fn', help='Input file name.',
                  default='ismi_graph.gpickle')
argp.add_argument('--nodes-file', dest='nodes_fn', help='Output nodes CSV file',
                  default='neo4j-nodes.csv')
argp.add_argument('--relations-file', dest='relations_fn', help='Output relations CSV file',
                  default='neo4j-relations.csv')
argp.add_argument('-l', '--log', dest='loglevel', choices=['INFO', 'DEBUG', 'ERROR'], default='INFO', 
                  help='Log level.')
args = argp.parse_args()

logging.basicConfig(level=args.loglevel)

# read networkx graph from pickle
logging.info(f"Reading graph from {args.input_fn}")
with open(args.input_fn, 'rb') as f:
    nx_graph = pickle.load(f)
    logging.info(f"Graph info: {nx_graph}")

# get field lists
node_fields = [':LABEL', ':ID', 'test'] + get_node_fields(nx_graph)
relation_fields = [':TYPE', ':START_ID', ':END_ID'] + get_relation_fields(nx_graph)

# write neo4j CSV import files
logging.info(f"Opening output node file {args.nodes_fn}")
with open(args.nodes_fn, 'w', newline='', encoding='utf-8') as csvfile:
    writer = csv.DictWriter(csvfile, fieldnames=node_fields, dialect=csv.excel, 
                            restval=None, quoting=csv.QUOTE_MINIMAL)
    writer.writeheader()
    n4j_nodes = write_nodes(nx_graph, writer)

# write neo4j CSV import files
logging.info(f"Opening output relation file {args.relations_fn}")
with open(args.relations_fn, 'w', newline='', encoding='utf-8') as csvfile:
    writer = csv.DictWriter(csvfile, fieldnames=relation_fields, dialect=csv.excel, 
                            quoting=csv.QUOTE_MINIMAL)
    writer.writeheader()
    write_relations(nx_graph, n4j_nodes, writer)

logging.info("Done.")
