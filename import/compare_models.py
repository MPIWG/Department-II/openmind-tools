import networkx as nx
import sys
import logging
import argparse
import pickle

__version__ = '1.6'

## configure behaviour

# name of type attribute
node_type_attribute = '_type'
rel_type_attribute = '_type'

# also compare attributes
check_attributes = True
check_attribute_content = False

# show details in info log
show_node_diff = False
show_rel_diff = False
show_att_diff = False

# types and attributes to ignore
ignore_node_types = {
}

ignore_attributes = {
    # PERSON
    'death_date_text', 'birth_date_text', 'hijra_date', 'num_death_date',
    'authority_id', 
    # TEXT
    'old_format', 'old_texttype', 'creation_date_num',
    # WITNESS
    'micro_filmed', 'is_annotated', 'msnum', 'notes_arabic', 'scans',
    'codex_part', 'source_of_information',
    # CODEX
    'is_alias', 'url_digitized_codex', 'indexmeta_folder', 'mpiwg_id', 'public',
    # PLACE
    'sub_type',
    # REFERENCE
    'id', 'endnote-content',
    # DIGITALIZATION
    'num_files',
}

ignore_type_attributes = {
    'SUBJECT': {'type'},
    'REFERENCE': {'source_of_information', '_label', 'endnote-id'},
    'FLORUIT_DATE': {'_label'},
    'ALIAS': {'_public'},
    'ROLE': {'_public'},
}

# relations to ignore
ignore_relations = {
    # PERSON
    'is_prime_alias_name_of',
    # COPY_EVENT
    'was_copied_in_as'
}

ignore_type_relations = {
    'was_copied_in': ['COPY_EVENT', 'REPOSITORY'],
    'was_studied_by': ['WITNESS', 'PERSON'],
    'is_a_transfer_of': ['TRANSFER_EVENT', 'WITNESS']
}


def compare_attributes(attrs1, attrs2, n):
    """compare two sets of attributes"""
    node_type = attrs1[node_type_attribute]

    for a in attrs1.keys():
        if a in ignore_attributes:
            continue
    
        if node_type in ignore_type_attributes:
            if a in ignore_type_attributes[node_type]:
                continue
        
        if a.startswith('_n_'):
            continue
        
        if a not in attrs2:
            val = attrs1[a]
            if isinstance(val, str):
                if 'date' in a:
                    # ignore date processing issues
                    if '"state":"not checked"' in val or 'unknown' in val:
                        continue
    
                if val.strip() == '':
                    # ignore only-whitespace
                    continue
            
            if show_att_diff:
                logging.info(f"attribute {a} missing in node graph2({n})")
            else:
                logging.debug(f"attribute {a} missing in node graph2({n})")
            logging.debug(f"val={attrs1[a]!r}")
            return False
        
        elif check_attribute_content:
            val1 = attrs1[a]
            val2 = attrs2[a]
            if isinstance(val1, str):
                val1 = val1.strip().replace('\r', '')
                val2 = val2.strip().replace('\r', '')
                
            if val1 != val2:
                if val1 in ['True', 'False']:
                    if (val1 == 'True' and val2 == True) or (val1 == 'False' and val2 == False):
                        continue
                elif val2 in ['True', 'False']:
                    if (val2 == 'True' and val1 == True) or (val2 == 'False' and val1 == False):
                        continue
                    
                if show_att_diff:
                    logging.info(f"attribute {a} different in node graph2({n})")
                    logging.debug(f"\n  {repr(val1)}\n vs \n  {repr(val2)}\n")
                else:
                    logging.debug(f"attribute {a} different in node graph2({n}): \n{repr(val1)}\n vs \n{repr(val2)}\n")
                return False
            
    for a in attrs2.keys():
        if a in ignore_attributes:
            continue
        
        if node_type in ignore_type_attributes:
            if a in ignore_type_attributes[node_type]:
                continue
        
        if a.startswith('_n_'):
            continue
        
        if a not in attrs1:
            val = attrs2[a]
            if isinstance(val, str):
                if 'date' in a:
                    # ignore date processing issues
                    if '"state":"not checked"' in val or 'unknown' in val:
                        continue
    
                if val.strip() == '':
                    # ignore only-whitespace
                    continue
            
            if show_att_diff:
                logging.info("attribute %s missing in node graph1(%s)"%(a, n))
            else:
                logging.debug("attribute %s missing in node graph1(%s)"%(a, n))
            logging.debug(f"val={val!r}")
            return False
        
    return True

def compare_nodes(nx_graph1, nx_graph2):
    """compare all nodes of two graphs"""
    
    logging.info("Comparing graph nodes...")
    cnt = 0
    missing_nodes1 = []
    missing_nodes2 = []
    attribute_differences = []
    # iterate all nodes in graph 1
    for n in nx.nodes(nx_graph1):
        if node_type_attribute not in nx_graph1.nodes[n]:
            raise KeyError("node %s in graph1 has no %s"%(n, node_type_attribute))
        
        if nx_graph1.nodes[n][node_type_attribute] in ignore_node_types:
            continue
        
        n2 = n
        if not nx_graph2.has_node(n2):
            # maybe other id type
            if isinstance(n, str):
                try:
                    n2 = int(n)
                except:
                    pass
            else:
                n2 = str(n)

            if not nx_graph2.has_node(n2):
                # still not right, ignore type swap
                n2 = n

        if not nx_graph2.has_node(n2):
            missing_nodes2.append(n2)
        else:
            attrs1 = nx_graph1.nodes[n]
            attrs2 = nx_graph2.nodes[n2]
            if check_attributes and not compare_attributes(attrs1, attrs2, n):
                attribute_differences.append(n)

    if len(missing_nodes2) > 0:
        logging.warning("%s nodes missing in graph 2"%len(missing_nodes2))
        #logging.debug("nodes: %s"%missing_nodes2)
        if show_node_diff:
            # TODO: fix index types 
            logging.info("nodes: %s"%([(n, nx_graph1.nodes[n]['_type']) for n in missing_nodes2]))
        else:
            logging.debug("nodes: %s"%([(n, nx_graph1.nodes[n]['_type']) for n in missing_nodes2]))
    
    # iterate all nodes in graph 2
    for n in nx.nodes(nx_graph2):
        if node_type_attribute not in nx_graph2.nodes[n]:
            raise KeyError("node %s in graph2 has no %s"%(n, node_type_attribute))
        
        if nx_graph2.nodes[n][node_type_attribute] in ignore_node_types:
            continue
        
        n1 = n
        if not nx_graph1.has_node(n1):
            # maybe other id type
            if isinstance(n, str):
                try:
                    n1 = int(n)
                except:
                    pass
            else:
                n1 = str(n)

            if not nx_graph1.has_node(n1):
                # still not right, ignore type swap
                n1 = n

        if not nx_graph1.has_node(n1):
            missing_nodes1.append(n1)

    if len(missing_nodes1) > 0:
        logging.warning("%s nodes missing in graph 1"%len(missing_nodes1))
        if show_node_diff:
            logging.info("nodes: %s"%(missing_nodes1))
        else:
            logging.debug("nodes: %s"%(missing_nodes1))
    
    if len(attribute_differences) > 0:
        logging.warning("%s nodes with attribute differences"%len(attribute_differences))
        if show_node_diff:
            logging.info("nodes: %s"%(attribute_differences))
        else:
            logging.debug("nodes: %s"%(attribute_differences))
    

def compare_relations(nx_graph1, nx_graph2):
    """compare relations of two graphs"""
    
    logging.info("Comparing graph relations...")
    cnt = 0
    missing_rels1 = []
    missing_rels2 = []
    # iterate all edges in graph 1
    for s, t in nx.edges(nx_graph1):
        rel_name = nx_graph1[s][t][0]['attr_dict'][rel_type_attribute]
        
        if rel_name in ignore_relations:
            continue
        
        if rel_name in ignore_type_relations:
            src_type = nx_graph1.nodes[s][node_type_attribute]
            tar_type = nx_graph1.nodes[t][node_type_attribute]
            ignore_src, ignore_tar = ignore_type_relations[rel_name]
            if src_type == ignore_src and tar_type == ignore_tar:
                continue
        
        s2 = s
        t2 = t
        if not nx_graph2.has_edge(s2, t2):
            # maybe other id type
            if isinstance(s2, str):
                try:
                    s2 = int(s2)
                    t2 = int(t2)
                except:
                    pass
            else:
                s2 = str(s2)
                t2 = str(t2)

            if not nx_graph2.has_edge(s2, t2):
                # still not right, ignore type swap
                s2 = s
                t2 = t

        if not nx_graph2.has_edge(s2, t2):
            missing_rels2.append((s2, rel_name, t2))

    if len(missing_rels2) > 0:
        logging.warning("%s relations missing in graph 2"%len(missing_rels2))
        if show_rel_diff:
            logging.info("relations: %s"%missing_rels2)
        else:
            logging.debug("relations: %s"%missing_rels2)
        #logging.debug("nodes: %s"%([nx_graph1.nodes[n] for n in missing_nodes2]))
    
    # iterate all edges in graph 2
    for s, t in nx.edges(nx_graph2):
        rel_name = nx_graph2[s][t][0]['attr_dict'][rel_type_attribute]
        
        if rel_name in ignore_relations:
            continue

        if rel_name in ignore_type_relations:
            src_type = nx_graph2.nodes[s][node_type_attribute]
            tar_type = nx_graph2.nodes[t][node_type_attribute]
            ignore_src, ignore_tar = ignore_type_relations[rel_name]
            if src_type == ignore_src and tar_type == ignore_tar:
                continue
        
        s1 = s
        t1 = t
        if not nx_graph1.has_edge(s1, t1):
            # maybe other id type
            if isinstance(s1, str):
                try:
                    s1 = int(s1)
                    t1 = int(t1)
                except:
                    pass
            else:
                s1 = str(s1)
                t1 = str(t1)

            if not nx_graph1.has_edge(s1, t1):
                # still not right, ignore type swap
                s1 = s
                t1 = t

        if not nx_graph1.has_edge(s1, t1):
            missing_rels1.append((s1,rel_name,t1))

    if len(missing_rels1) > 0:
        logging.warning("%s relations missing in graph 1"%len(missing_rels1))
        if show_rel_diff:
            logging.info("relations: %s"%(missing_rels1))
        else:
            logging.debug("relations: %s"%(missing_rels1))
    

## main
argp = argparse.ArgumentParser(description='Compare two networkx graphs with ISMI data.')
argp.add_argument('--version', action='version', version='%(prog)s ' + __version__)
argp.add_argument('file1', help='First networkx graph in gpickle format',
                  default='ismi_graph1.gpickle')
argp.add_argument('file2', help='Second networkx graph in gpickle format',
                  default='ismi_graph2.gpickle')
argp.add_argument('--ignore-attributes', dest='ignore_attributes', 
                  help='list of attributes to ignore on all entities')
argp.add_argument('--show-nodes', dest='show_nodes', action='store_true', 
                  help='show node differences in detail')
argp.add_argument('--show-relations', dest='show_rels', action='store_true', 
                  help='show relation differences in detail')
argp.add_argument('--show-attributes', dest='show_atts', action='store_true', 
                  help='show attribute differences in detail')
argp.add_argument('--check-attribute-content', dest='check_att_content', action='store_true', 
                  help='check attribute content (optional)')
argp.add_argument('-l', '--log', dest='loglevel', choices=['INFO', 'DEBUG', 'ERROR'], default='INFO', 
                  help='Log level.')
args = argp.parse_args()

logging.basicConfig(level=args.loglevel)

if args.ignore_attributes:
    ignore_attributes.update(set(args.ignore_attributes.split(',')))

show_node_diff = args.show_nodes
show_rel_diff = args.show_rels
show_att_diff = args.show_atts
check_attribute_content = args.check_att_content

input1_fn = args.file1
input2_fn = args.file2

# read networkx graph from pickle
logging.info("Reading graph 1 from %s"%input1_fn)
with open(input1_fn, 'rb') as f:
    nx_graph1 = pickle.load(f)
    logging.info("Graph 1 info: %s"%(nx_graph1))

logging.info("Reading graph 2 from %s"%input2_fn)
with open(input2_fn, 'rb') as f:
    nx_graph2 = pickle.load(f)
    logging.info("Graph 2 info: %s"%(nx_graph2))

# operate    
compare_nodes(nx_graph1, nx_graph2)
compare_relations(nx_graph1, nx_graph2)


logging.info("Done.")
