import xml.etree.ElementTree as ET
import sys
import logging
import argparse

__version__ = '1.1'

# node types to exclude from the graph
# exclude_objects_of_type = ['DIGITALIZATION', 'REFERENCE']
exclude_objects_of_type = []


def new_tree(root, ent_elem, rel_elem):
    """Creates XML DOM for new entity type.
       Returns the DOM tree.
    """
    tree = ET.ElementTree()
    # create new root element
    new_root = ET.Element(root.tag, root.attrib)
    # set new root
    tree._setroot(new_root)
    # create new entity container element
    new_ent_elem = ET.SubElement(new_root, ent_elem.tag, ent_elem.attrib)
    # create new entity container element
    new_rel_elem = ET.SubElement(new_root, rel_elem.tag, rel_elem.attrib)
    # return element reference
    return {'tree': tree, 'root': new_root, 'ent_cnt': 0, 'rel_cnt': 0, 
            'ents_elem': new_ent_elem, 'rels_elem': new_rel_elem}


def import_ents(root, ents_elem, rels_elem, trees):
    """Import all entities from etree element elem.
    Returns DOM trees dict.
    """
    cnt = 0
    xml_num = ents_elem.get('count')
    logging.info(f"XML says {xml_num} entities. Processing...")
    
    # iterate through entities element
    for ent_elem in ents_elem:
        cnt += 1
        
        oc = ent_elem.get('object-class')
        if oc in exclude_objects_of_type:
            # skip this entity
            continue
        
        if (not oc in trees):
            # create new output dom tree
            trees[oc] = new_tree(root, ents_elem, rels_elem)
            
        target_elem = trees[oc]['ents_elem']
        target_elem.append(ent_elem)
        trees[oc]['ent_cnt'] += 1;

    return trees


def import_rels(rels_elem, trees):
    """Import all entities from etree element elem.
    Returns DOM trees dict.
    """
    cnt = 0
    xml_num = rels_elem.get('count')
    logging.info(f"XML says {xml_num} relations. Processing...")
    
    # iterate through entities element
    for rel_elem in rels_elem:
        cnt += 1

        oc = rel_elem.get('source-class')
        if oc in exclude_objects_of_type:
            # skip this entity
            continue
        
        if (not oc in trees):
            logging.error(f"Relation source class unknown: {oc}")
            continue
            
        target_elem = trees[oc]['rels_elem']
        target_elem.append(rel_elem)
        trees[oc]['rel_cnt'] += 1;
        
    return trees


def import_all(input_fn):
    """Parse XML file input_fn and return DOM trees dict.
    """
    # parse XML file
    logging.info(f"parsing XML file {input_fn}")
    tree = ET.parse(input_fn)
    logging.debug("etree ready")
    root = tree.getroot()
    ents = root.find('entities')
    rels = root.find('relations')
    # import and process
    trees = {}
    trees = import_ents(root, ents, rels, trees)
    trees = import_rels(rels, trees)
    return trees

  
def exportAll(trees, output_pat):
    """Write DOM trees into XML files"""  
    for oc in trees.keys():
        # update counts
        ent_elem = trees[oc]['ents_elem']
        ent_cnt = trees[oc]['ent_cnt']
        ent_elem.set('count', str(ent_cnt))
        rel_elem = trees[oc]['rels_elem']
        rel_cnt = trees[oc]['rel_cnt']
        rel_elem.set('count', str(rel_cnt))
        # save tree
        tree = trees[oc]['tree']
        fn = output_pat % (oc.lower())
        logging.info(f"writing XML file {fn} ({ent_cnt} ents, {rel_cnt} rels)")
        tree.write(fn, encoding='utf-8', xml_declaration=True)


## main
argp = argparse.ArgumentParser(description='Split OpenMind-XML into per-object XML files.')
argp.add_argument('--version', action='version', version='%(prog)s ' + __version__)
argp.add_argument('input_fn', help='Input file name.',
                  default='openmind-data.xml')
argp.add_argument('output_pat', nargs='?', help='Output file name pattern (%s type placeholder)',
                  default='openmind-data-%s.xml')
argp.add_argument('-l', '--log', dest='loglevel', choices=['INFO', 'DEBUG', 'ERROR'], default='INFO', 
                  help='Log level.')
args = argp.parse_args()

logging.basicConfig(level=args.loglevel)

logging.info(f"Reading OpenMind-XML file {args.input_fn}")
if len(exclude_objects_of_type) > 0:
    logging.info(f"  skipping objects of type {exclude_objects_of_type}")
    
# read input file
trees = import_all(args.input_fn)
# write output files
exportAll(trees, args.output_pat)

logging.info("Done.")
