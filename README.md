# openmind-tools

Utilities to work with openmind data:

* Python tools to work with OpenMind
    * split XML dumps
    * convert XML dumps
    * import XML dumps into Neo4J
    * analyze transaction log files
* Docker setup to run Neo4J and import OpenMind data

## Installation

Check out in your homedirectory as "openmind-tools" (or change `TOOLS_DIR` in the scripts):
```
git clone 'git@gitlab.gwdg.de:MPIWG/Department-II/openmind-tools.git'
```

## Neo4J

Neo4J is installed in the `neo4j` subdirectory.

Create directories `server/certificates/c2s_policy/trusted` and `server/certificates/c2s_policy/revoked`.

Put the SSL private key (PEM format) in `server/certificates/c2s_policy/private.key` and the certificate (PEM format with certificate chain) both in `server/certificates/c2s_policy/public.crt` and in `server/certificates/c2s_policy/trusted`.

If you want the server private key to be more secret you can set the permissions to only be readable to user 101 or group 101.

Start the Neo4J container in the `openmind-tools/neo4j` directory with
```
./start-neo4j.sh
```

Open the Neo4J frontend in your browser:

https://yourhostname:7474/browser/

Enter the default password "neo4j" and change it to a safer password.

## Python

Install a Python3 virtual environment in `openmind-tools/python-env`:
```
python3 -m venv python-env
```

Install the NetworkX Python package:
```
./python-env/bin/pip install networkx
```

## Import ISMI OpenMind data in Neo4J

Now you can run
```
./neo4j/import-ismi-xml.sh
```
to import the OpenMind XML dump from `/var/lib/ismi/daily-data/openmind-data.xml` into Neo4J.
