#!/bin/bash

if [ -f .env ] ; then
    source .env
fi

TOOLS_DIR=${TOOLS_DIR:-$HOME/openmind-tools}
N4J_DIR=$TOOLS_DIR/neo4j
cd $N4J_DIR

ISMIDIR=${ISMIDIR:-/var/lib/ismi/daily-data}

# create new import volume
docker volume create n4j-import

echo "Converting OpenMind XML dump to Neo4J import files..."
docker compose run --rm --volume="$ISMIDIR:/ismi-data" --volume="n4j-import:/data" \
    --entrypoint="/bin/bash" tools /scripts/convert.sh

if [ $? -ne 0 ]
then
    echo "ERROR converting XML dump!"
    exit 1
fi

echo "Stopping N4J server..."
docker compose stop neo4j

# remove old neo4j db
echo "Importing data into N4J..."
docker compose run --rm --volume="n4j-import:/import" neo4j \
    /scripts/import.sh

# clean up
docker volume rm n4j-import

# re-start neo4j server
echo "Starting N4J server..."
docker compose start neo4j
