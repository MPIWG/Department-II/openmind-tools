#!/bin/bash

if [ -f .env ] ; then
    source .env
fi

TOOLS_DIR=${TOOLS_DIR:-$HOME/openmind-tools}
N4J_DIR=$TOOLS_DIR/neo4j

cd $N4J_DIR
mkdir -p server/data
mkdir -p server/certificates

echo "Starting N4J server..."
docker compose up -d
