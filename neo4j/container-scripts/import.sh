#!/bin/bash

# this script needs to run inside the tools container!

set -e

# import new database
neo4j-admin database import full \
  --nodes=/import/neo4j-nodes.csv \
  --relationships=/import/neo4j-relations.csv \
  --overwrite-destination=true \
  --multiline-fields=true
