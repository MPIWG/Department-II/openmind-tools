#!/bin/bash

# this script needs to run inside the tools container!

set -e
cd /data
# convert XML dump to networkx model
python3 /app/import/ismixml2model.py /ismi-data/openmind-data.xml ismi_graph.gpickle -public

# convert model to model with places 
#cp /ismi-data/ismi_places_loc.csv .
#python3 /app/import/model2model.py

# create neo4j import files
python3 /app/import/model2neo4j_import.py ismi_graph.gpickle neo4j-nodes.csv neo4j-relations.csv
